$( "#confirm_passwd" ).prop( "disabled", true );
$( "#submit" ).prop("disabled", true);

$('#pseudo').on( "change", function() {
      let value = $(this).val().length;
      if(value>=6)
      {
          $(this).attr("style","color : lightgreen ");
      }
      else {
          
          $(this).attr("style","color : red ");
          
      }
});

$('#login').on( "change", function() {
    let value = $(this).val();

    if (isEmail(value) == true){
        $(this).attr("style","color : lightgreen ");
      
    }
    else{
        $(this).attr("style","color : red ");
      
    }
});

$("#passwd").on( "change" ,function()
    {
        let strength = 3;
        let valueLength = $(this).val().length;
        let value = $(this).val();
        let msg_final = "";
        let msg_length = "Le mot de passe doit contenir au moins 8 caractères";
        let msg_chiffre = "\nLe mot de passe doit contenir au moins 1 chiffre";
        let msg_lettre = "\nLe mot de passe doit contenir une majuscule et une minuscule";
        if(valueLength<8)
        {
            strength -=1;
            msg_final = msg_final + msg_length;
        }
        if(!value.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))
        {
            strength -=1;
            msg_final = msg_final + msg_lettre;
        }
        if(!value.match(/([0-9])/))
        {
            strength -=1;
            msg_final = msg_final + msg_chiffre;
        }
        if(strength<3)
        {
            $(this).attr("style","color : red");
            alert(msg_final);
            $( "#confirm_passwd" ).prop( "disabled", true );
            $( "#submit" ).prop("disabled", true);
        }
        else
        {
            $(this).attr("style","color : lightgreen");
            $( "#confirm_passwd" ).prop( "disabled", false );
        }



    });


$('#confirm_passwd').on( "change", function() {
    let value = $(this).val();

    if (checkBothpasswd(value) == true){
        $(this).attr("style","color : lightgreen ");
        $(this).attr("style","border-color : lightgreen ");
        $( "#submit" ).prop("disabled", false);
    }
    else{
        $(this).attr("style","color : red ");
        $(this).attr("style","border-color : lightgreen ");
        $( "#submit" ).prop("disabled", true);
    }
});

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
  }


function checkBothpasswd(passwd){
    let value = $( "#passwd" ).val();
    return passwd == value;
}