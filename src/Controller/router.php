<?php
   
    require_once($_SERVER['DOCUMENT_ROOT']."/crud-rkv/src/Model/DataBase/DataBase.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/crud-rkv/src/Model/Entities/User.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/crud-rkv/src/Controller/Entities/UserController.php");

    $request_method = $_SERVER["REQUEST_METHOD"];
    $userController = new UserController();
    switch($request_method)
    {
        case 'GET':
            if(!empty($_GET["pseudo"]))
            {

                // Récupérer un seul produit
                $pseudo = $_GET["pseudo"];
                $userController->getAction($pseudo);
            }
            else
            {
                // Récupérer tous les produits
                $userController->getAllAction();
            }
            break;
        case 'POST':
            $values = json_decode(file_get_contents('php://input'), TRUE);

            if(isset($values['pseudo']) && isset($values['login']) && isset($values['passwd'])){
                $user = new User($values['pseudo'], $values['login'], $values['passwd']);
                $userController->postAction($user);
            }else{
                echo "pouetouèdfte";
            }
            break;
        case 'PUT':
            $values = json_decode(file_get_contents('php://input'), TRUE);
            if(!empty($values['pseudo']) && !empty($values['login']) && !empty($values['passwd'])){
                $user = new User($values['pseudo'], $values['login'], $values['passwd']);
                $userController->putAction($user);
            }else{
                echo "pouette";
            }
            break;
        case 'DELETE':
            $values = json_decode(file_get_contents('php://input'), TRUE);
            if(!empty($values["pseudo"]))
            {
                $userController->deleteAction($values["pseudo"]);
            }
            else
            {
                // Récupérer tous les produits
                echo "pouette";
            }
            break;
        default:
            // Requête invalide
            header("HTTP/1.0 405 Method Not Allowed");
            break;
    }