<?php
require_once($_SERVER['DOCUMENT_ROOT']."/crud-rkv/src/Model/DataBase/DataBase.php");

class UserController
{

    public function getAction(string $pseudo)
    {
        $myDb = new DataBase("root", "", "localhost", "3306", "exo_connect");
        $myCo = $myDb->getConnection();

        $statement = $myCo->prepare("SELECT * FROM user WHERE pseudo=:pseudo");
        $statement->bindParam('pseudo', $pseudo);

        try{
            if($statement->execute()){
                if($result = $statement->fetch()){
                    $return_array = array();
                    $return_array[] = array(
                        "id" => $result['id'],
                        "pseudo" => $result['pseudo'],
                        "login" => $result['login'],
                        "passwd" => $result['passwd'],
                        "creationDate" => $result['creationDate'],
                        "lastConnexionDate" => $result['lastConnexionDate'],

                    );
                    //code_response
                    //cache_control

                    header($_SERVER['SERVER_PROTOCOL'].http_response_code()."Success");
                    header("Cache-Control: public, no-cache");
                    header('Content-Type: application/json');

                    echo json_encode($return_array);
                }else{
                    http_response_code(404);

                    $return_array = array("error_code" => http_response_code(), "error_message" => "User not found");

                    header("HTTP\/1.1 404 Not Found");
                    header("Cache-Control: public, no-cache");
                    header('Content-Type: application/json');

                    echo json_encode($return_array);
                }
            }else{
                http_response_code(404);
                $return_array = array("error_code" => http_response_code(), "error_message" => "DataBase not found");

                header("HTTP\/1.1 404 DataBase Not Found");
                header("Cache-Control: public, no-cache");
                header('Content-Type: application/json');

                echo json_encode($return_array);
            }
        }
        catch(\Exception $e){
            http_response_code(500);
            $return_array = array("error_code" => http_response_code(), "error_message" => "Server error, please retry later");

            header("HTTP\/1.1 404 DataBase Not Found");
            header("Cache-Control: public, no-cache");
            header('Content-Type: application/json');

            return json_encode($e);
        }
    }

    public function getAllAction()
    {
        $myDb = new DataBase("root", "", "localhost", "3306", "exo_connect");
        $myCo = $myDb->getConnection();

        $statement = $myCo->prepare("SELECT * FROM User");


        try{
            if($statement->execute()) {
                if ($result = $statement->fetch()) {
                    $statement->execute();
                    $result = $statement->fetchAll();
                    $return_array = array();
                    foreach ($result as $res) {
                        $return_array[] = array(
                            "id" => $res['id'],
                            "pseudo" => $res['pseudo'],
                            "login" => $res['login'],
                            "passwd" => $res['passwd'],
                            "creationDate" => $res['creationDate'],
                            "lastConnexionDate" => $res['lastConnexionDate'],
                        );
                    }
                    header($_SERVER['SERVER_PROTOCOL'].http_response_code()."Success");
                    header("Cache-Control: public, no-cache");
                    header('Content-Type: application/json');

                    echo json_encode($return_array);
                }else{
                    http_response_code(404);

                    $return_array = array("error_code" => http_response_code(), "error_message" => "Users not found");

                    header("HTTP\/1.1 404 Not Found");
                    header("Cache-Control: public, no-cache");
                    header('Content-Type: application/json');

                    echo json_encode($return_array);
                }
            }else{
                http_response_code(404);
                $return_array = array("error_code" => http_response_code(), "error_message" => "DataBase not found");

                header("HTTP\/1.1 404 DataBase Not Found");
                header("Cache-Control: public, no-cache");
                header('Content-Type: application/json');

                echo json_encode($return_array);
            }
        }
        catch(\Exception $e){
            http_response_code(500);
            $return_array = array("error_code" => http_response_code(), "error_message" => "Server error, please retry later");

            header("HTTP\/1.1 404 DataBase Not Found");
            header("Cache-Control: public, no-cache");
            header('Content-Type: application/json');

            echo json_encode($e);
        }
    }

    public function postAction(User $user)
    {
        $myDb = new DataBase("root", "", "localhost", "3306", "exo_connect");
        $myCo = $myDb->getConnection();

        try{
            $check_datas = $myCo->prepare("SELECT * FROM user WHERE pseudo=:pseudo or login=:login");

            $pseudo = $user->getPseudo();
            $login = $user->getLogin();

            $check_datas->bindParam('pseudo', $pseudo);
            $check_datas->bindParam('login', $login);

            if($check_datas->execute()){
                if($check_datas->rowCount() > 0){
                    http_response_code(500);
                    $return_array = array("error_code" => http_response_code(), "error_message" => "Create User : Data already used");

                    header("HTTP\/1.1 500 DataBase Not Found");
                    header("Cache-Control: public, no-cache");
                    header('Content-Type: application/json');

                    echo json_encode($return_array);
                }else{
                    $statement = $myCo->prepare("INSERT INTO User (pseudo,login, passwd,creationDate,lastConnexionDate) VALUES ('".$user->getPseudo()."','".$user->getLogin()."','".md5($user->getPsd())."',".$user->getDateCreation().",".$user->getDateDerConnexion().")");
                    try{
                        if($statement->execute()){
                            header($_SERVER['SERVER_PROTOCOL'].http_response_code()."Success");
                            header("Cache-Control: public, no-cache");
                            header('Content-Type: application/json');

                            echo json_encode("created");
                        }

                    }catch(\Exception $e){
                        header('Content-Type: application/json');
                        echo json_encode($e);
                    }
                }
            }else{
                echo $check_datas->errorInfo();
            }
        }
        catch(\Exception $e){
            echo $e;
        }
    }

    public function putAction(User $user)
    {
        $myDb = new DataBase("root", "", "localhost", "3306", "exo_connect");
        $myCo = $myDb->getConnection();

        try{
            $pseudo = $user->getPseudo();
            $check_datas = $myCo->prepare("SELECT * FROM user WHERE pseudo=:pseudo");
            $check_datas->bindParam('pseudo', $pseudo);

            if($check_datas->execute()){
                if($check_datas->rowCount() > 0){

                    $check_login = $myCo->prepare("SELECT * FROM user WHERE  login=:login");
                    $login = $user->getLogin();
                    $check_login->bindParam("login", $login);
                    if($check_login->execute()){
                        if($check_login->rowCount() > 0){
                            http_response_code(500);
                            $return_array = array("error_code" => http_response_code(), "error_message" => "Update User : Data already used");

                            header("HTTP\/1.1 500 Update Failed");
                            header("Cache-Control: public, no-cache");
                            header('Content-Type: application/json');
                            echo json_encode($return_array);
                        }else{
                            $statement = $myCo->prepare("UPDATE user SET login='".$user->getLogin()."', passwd='".md5($user->getPsd())."' WHERE pseudo=:pseudo");
                            $statement->bindParam("pseudo", $pseudo);
                            try{
                                if($statement->execute()){
                                    header($_SERVER['SERVER_PROTOCOL'].http_response_code()."Success");
                                    header("Cache-Control: public, no-cache");
                                    header('Content-Type: application/json');

                                    echo json_encode("updated");
                                }

                            }catch(\Exception $e){
                                http_response_code(500);
                                $return_array = array("error_code" => http_response_code(), "error_message" => "Update User : Update Failed");

                                header("HTTP\/1.1 500 Update Failed");
                                header("Cache-Control: public, no-cache");
                                header('Content-Type: application/json');
                                echo json_encode($return_array);
                            }
                        }
                    }
                }else{
                    http_response_code(500);
                    $return_array = array("error_code" => http_response_code(), "error_message" => "Update User : User doesn\'t exist");

                    header("HTTP\/1.1 500 DataBase Not Found");
                    header("Cache-Control: public, no-cache");
                    header('Content-Type: application/json');

                    echo json_encode($return_array);
                }
            }else{
                http_response_code(500);
                $return_array = array("error_code" => http_response_code(), "error_message" => "Update User : User doesn\'t exist");

                header("HTTP\/1.1 500 DataBase Not Found");
                header("Cache-Control: public, no-cache");
                header('Content-Type: application/json');

                echo json_encode($return_array);
            }
        }
        catch(\Exception $e){
            echo $e;
        }

    }

    public function deleteAction(string $pseudo)
    {
        $myDb = new DataBase("root", "", "localhost", "3306", "exo_connect");
        $myCo = $myDb->getConnection();

        $statement = $myCo->prepare("DELETE FROM User WHERE pseudo=:pseudo");
        $statement->bindParam('pseudo', $pseudo);

        try{
            if($statement->execute()){

                header($_SERVER['SERVER_PROTOCOL'].http_response_code()."Success");
                header("Cache-Control: public, no-cache");
                header('Content-Type: application/json');

                echo json_encode("deleted");
            }else{
                http_response_code(500);
                $return_array = array("error_code" => http_response_code(), "error_message" => "Delete User : Delete failed.");

                header("HTTP\/1.1 500 DataBase Not Found");
                header("Cache-Control: public, no-cache");
                header('Content-Type: application/json');

                echo json_encode($return_array);
            }

        }catch (\Exception $e){
            http_response_code(500);
            $return_array = array("error_code" => http_response_code(), "error_message" => "Delete User : Delete failed.");

            header("HTTP\/1.1 500 DataBase Not Found");
            header("Cache-Control: public, no-cache");
            header('Content-Type: application/json');

            echo json_encode($return_array);

        }
    }

}