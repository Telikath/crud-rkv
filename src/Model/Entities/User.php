<?php

class User
{
    private $id;
    private $pseudo;
    private $login;
    private $psd;
    private $dateCreation;
    private $dateLastConn;


    //Constructor
    /**
     * User constructor.
     * @param $pseudo
     * @param $login
     * @param $psd
     */
    public function __construct($pseudo, $login, $psd){
        $this->pseudo = $pseudo;
        $this->login = $login;
        $this->psd = $psd;
        $this->dateCreation = time();
        $this->dateLastConn = 0;
    }

    public function setId(){
        $db = new DataBase('root','','localhost','3306','exo_connect');
        $conn = $db->getConnection();
        if($this->login != ""){
            $statement = $conn->prepare('SELECT id FROM user WHERE login = '.$this->getLogin());
            try{
                $req = $statement->execute();
                if($res = $req->fetch()){
                    $this->id  = $res;
                }
            }catch(\Exception $e){
                return $e;
            }
        }
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPseudo()
    {
        return $this->pseudo;
    }

    //Getters
    /**
     * @return string
     */
    public function getLogin(){
        return $this->login;
    }
    /**
     * @return string
     */
    public function getPsd(){
        return $this->psd;
    }
    /**
     * @return integer
     */
    public function getDateCreation(){
        return $this->dateCreation;
    }
    /**
     * @return integer
     */
    public function getDateDerConnexion(){
        return $this->dateLastConn;
    }




    //Setters
    /**
     * @param string $pseudo
     */
    public function setPseudo($pseudo)
    {
        $this->pseudo = $pseudo;
    }
    /**
     * @param string $login
     */
    public function setLogin($login){
        $this->login = $login;
    }
    /**
     * @param string $psd
     */
    public function setPsd($psd){
        $this->psd = $psd;
    }
    /**
     * @param string $dateCreation
     */
    public function setDateCreation($dateCreation){
        $this->dateCreation = $dateCreation;
    }
    /**
     * @param string $dateLastConn
     */
    public function setDateDerConnexion($dateLastConn){
        $this->dateLastConn = $dateLastConn;
    }

    public function getAllValues(){
        return array($this->pseudo, $this->login, $this->psd, $this->dateCreation, $this->dateLastConn);
    }
    public function getColumnNames(){
        return array( "pseudo", "login", " passwd", "creationDate", "lastConnexionDate");
    }
}