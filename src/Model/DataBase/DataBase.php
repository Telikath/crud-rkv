<?php

class DataBase
{
    private $user;
    private $passwd;
    private $addr;
    private $port;
    private $dbName;

    /**
     * DataBaseConnector constructor.
     * @param $user
     * @param $passwd
     * @param $addr
     * @param $port
     * @param $dbName
     */
    public function __construct($user, $passwd, $addr, $port, $dbName)
    {
        $this->user = $user;
        $this->passwd = $passwd;
        $this->addr = $addr;
        $this->port = $port;
        $this->dbName = $dbName;
    }
    public function getConnection(){
        return new \PDO('mysql:host='.$this->addr.';dbname='.$this->dbName, $this->user, $this->passwd);
    }
}
/*
$myDB = new DataBase("root", "", "localhost", "3006","exo_connect");

var_dump($myDB);*/