#---------------------------------------------
    Instructions for the RestFull Api

    Edited by édited by Rémi CHIANEA 04.04.2021
#---------------------------------------------

    URL : collaborativeex/src/Controller/router.php

    GET :
        /---------------------------------------------------/
       /- GET collaborativeex/src/Controller/router.php ---/
      /---------------------------------------------------/

            In this case, your request will return all the elements
            in the table named "user".

        /--------------------------------------------------------/
       /- GET collaborativeex/src/Controller/router.php?pseudo=$1 --/
      /--------------------------------------------------------/


            In this case, your request will return the element
            whose pseudo is $1 in the table named "user".

    POST :
        Here, the post request was designed to create users in the associated table.

        /---------------------------------------------------/
       /- POST collaborativeex/src/Controller/router.php --/
      /---------------------------------------------------/

            In this case, you have to put the following parameter
            in JSON format.
                    {
                        "pseudo" = "yourPseudo",
                        "login" = "yourLogin",
                        "psd" = "yourPassword"
                    }
            The answer to this kind of request when everything worked well is :
            "created"

    PUT :
        Here, the put request was designed to update users in the associated table.

         /---------------------------------------------------/
        /- PUT collaborativeex/src/Controller/router.php ---/
       /---------------------------------------------------/

            In this case, you have to put the following parameter
            in JSON format.
                    {
                        "pseudo" = "yourPseudo",
                        "login" = "yourLogin",
                        "psd" = "yourPassword"
                    }
            The answer to this kind of request when everything worked well is :
            "updated"

    DELETE :
        Here, the delete request was designed to delete users in the associated table.

          /---------------------------------------------------/
         /- DELETE collaborativeex/src/Controller/router.php-/
        /---------------------------------------------------/

            In this case, you have to put the following parameter
            in JSON format.
                    {
                        "pseudo" = "yourPseudo",
                    }
            The answer to this kind of request when everything worked well is :
            "deleted"